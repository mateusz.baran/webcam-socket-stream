import logging

from connection.user import Client
from baselines.webcam import Webcam

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    client = Client(("localhost", 35000))
    connection = client.connect()

    if connection:
        webcam = Webcam(connection)
        webcam.receive()
