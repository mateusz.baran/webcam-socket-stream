import logging

import cv2
import numpy as np
import pickle as pkl

from connection.interface import SocketCommunication


class Webcam(SocketCommunication):
    def __init__(self, connection, name='Webcam', window_size=(800, 600), delay=10):
        SocketCommunication.__init__(self, connection)
        self.logger = logging.getLogger(__name__)
        self.name = name
        self.window_size = window_size
        self.exit = False
        self.delay = delay
        self.cap = None
        self.flip = True

    def release_cap(self):
        if self.cap is not None:
            self.logger.debug(f'realise video capture')
            self.cap.release()

    def destroy_window(self):
        self.logger.debug(f'destroy window: {self.name}')
        cv2.destroyWindow(self.name)

    def receive(self):
        self.logger.debug(f'init window: {self.name}, window size: {self.window_size}')
        cv2.namedWindow(self.name, cv2.WINDOW_GUI_NORMAL)
        cv2.resizeWindow(self.name, *self.window_size)

        while not self.exit and self.connection.send(b'') and (
                cv2.getWindowProperty(self.name, cv2.WND_PROP_VISIBLE) and
                cv2.getWindowProperty(self.name, cv2.WND_PROP_FULLSCREEN) in
                (cv2.WINDOW_NORMAL, cv2.WINDOW_FULLSCREEN)):
            frame = self.connection.recv()
            if frame is None:
                break

            frame = decode(pkl.loads(frame))
            if self.flip:
                frame = np.flip(frame, 1)

            cv2.imshow(self.name, frame)
            self.key_events(cv2.waitKey(self.delay))

        self.destroy_window()

    def transmit(self, device_id=0):
        self.release_cap()
        self.logger.debug(f'init video capture, device id: {device_id}')
        self.cap = cv2.VideoCapture(device_id)

        while self.connection.recv() is not None:
            ret, frame = self.cap.read()
            if frame is None or not self.connection.send(pkl.dumps(encode(frame))):
                break

    def key_events(self, key):
        if key == 32:
            self.flip = not self.flip
        elif key == 27:
            self.exit = True


def encode(img):
    return cv2.imencode('.jpg', img)[1]


def decode(img):
    return cv2.imdecode(img, cv2.IMREAD_COLOR)


def devices():
    dev = []
    for i in range(10):
        cap = cv2.VideoCapture(i)
        if cap is not None:
            if not cap.isOpened():
                cap.release()
            else:
                dev.append(i)
    return dev
