import logging

from connection.user import Server
from baselines.webcam import Webcam

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    server = Server(("", 35000))

    if server.listen():
        connection = server.accept()
        if connection:
            webcam = Webcam(connection)
            webcam.transmit()
