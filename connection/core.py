import socket
import logging


class Connection:
    BUFFER_SIZE = 4096
    BYTES = 8
    BYTEORDER = 'big'
    BYTE_ZERO = b'\x00'

    def __init__(self, socket, address):
        self.logger = logging.getLogger(__name__)
        self.socket = socket
        self.address = address

    def close(self):
        self.logger.info(f'Disconnected {self.address[0]}:{self.address[1]}')
        self.socket.close()

    def int_to_bytes(self, x):
        x = x.to_bytes((x.bit_length() + 7) // 8, self.BYTEORDER)
        return self.BYTE_ZERO * (self.BYTES - len(x)) + x

    def int_from_bytes(self, x):
        return int.from_bytes(x, self.BYTEORDER)

    def send(self, data):
        try:
            self.socket.sendall(self.int_to_bytes(len(data)) + data)
            return True
        except Exception as e:
            self.logger.debug(f'Send error {str(e)}')

    def recv(self):
        try:
            total_size = self._recv_bytes(self.BYTES)
            total_size = self.int_from_bytes(total_size)
            data = self._recv_bytes(total_size)

            return data
        except Exception as e:
            self.logger.debug(f'Receive error {str(e)}')

    def _recv_bytes(self, size, data=b''):
        left_bytes = size - len(data)
        while left_bytes:
            buffer_size = self.BUFFER_SIZE if left_bytes > self.BUFFER_SIZE else left_bytes
            batch = self.socket.recv(buffer_size)
            if not batch:
                raise OSError('NOT DATA')

            data += batch
            left_bytes = size - len(data)

        return data


class Socket:
    def __init__(self, address):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.address = address_validation(address)
        self.logger = logging.getLogger(__name__)

    def close(self):
        self.logger.debug('Closing socket')
        self.socket.shutdown(socket.SHUT_WR)
        self.socket.close()


def address_validation(address):
    assert type(address) == tuple, 'address must be tuple'
    assert len(address) == 2, 'address must has 2 items'
    assert type(address[0]) == str and type(address[1]) == int, 'address must be (ip:str, port:int)'
    assert 0 <= address[1] < 65536, 'port must be between 0 and 65535'

    port = address[1]

    try:
        ip = socket.gethostbyname(address[0])
    except OSError:
        ip = ''

    return ip, port


def random_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 0))
    ip, port = s.getsockname()
    s.close()
    return port


def check_port(port):
    assert type(port) == int, 'port must be int'

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.bind(('', port))
        s.close()
        return True
    except (OSError, OverflowError):
        s.close()
        return False
