import socket

from connection.core import Connection, Socket


class Server(Socket):
    def __init__(self, address):
        super().__init__(address)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def listen(self):
        try:
            self.socket.bind(self.address)
        except OSError:
            self.logger.info(f'Bind error {self.address[0]}:{self.address[1]}')
            return False
        else:
            self.socket.listen(5)
            self.logger.info(f'Listening {self.address[0]}:{self.address[1]}')
            return True

    def accept(self):
        try:
            connection, client_address = self.socket.accept()
            self.logger.info(f'Connected by {client_address[0]}:{client_address[1]}')
            return Connection(connection, client_address)
        except OSError:
            self.logger.error(f'Cannot accept incoming connection. Check address bind.')


class Client(Socket):
    def __init__(self, address):
        super().__init__(address)

    def connect(self):
        try:
            self.socket.connect(self.address)
        except OSError:
            self.logger.info(f'Failed connection to {self.address[0]}:{self.address[1]}')
        else:
            self.logger.info(f'Connected to {self.address[0]}:{self.address[1]}')
            return Connection(self.socket, self.address)
